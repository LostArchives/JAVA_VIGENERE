# Vigenere Project Encrypt/Decrypt

This project was about to create a java application able to crypt and decrypt using the Vigene cypher.

The following functionalities are present :

-Encrypt a text

-Decrypt a text

-Find the keySize

-Find the keyValue

# Structure

External files should be stored in the "resources" folder.

Generated files should be stored in the "output" folder.

You already have working examples in these folders.

The class used to test is called "TestCryptAnalyse" and is in the package called "test".

# Installation

You just have to copy the project into your Eclipse Workspace and import it
